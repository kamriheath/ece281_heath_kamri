--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : top_basys3.vhd
--| AUTHOR(S)     : Capt Phillip Warner
--| CREATED       : 3/9/2018  MOdified by C3C Kamri Heath and C3C Kevin Carrig (4/16/2021)
--| DESCRIPTION   : This file implements the top level module for a BASYS 3 to 
--|					drive the Lab 4 Design Project (Advanced Elevator Controller).
--|
--|					Inputs: clk       --> 100 MHz clock from FPGA
--|							btnL      --> Rst Clk
--|							btnR      --> Rst FSM
--|							btnU      --> Rst Master
--|							btnC      --> GO (request floor)
--|							sw(15:12) --> Passenger location (floor select bits)
--| 						sw(3:0)   --> Desired location (floor select bits)
--| 						 - Minumum FUNCTIONALITY ONLY: sw(1) --> up_down, sw(0) --> stop
--|							 
--|					Outputs: led --> indicates elevator movement with sweeping pattern (additional functionality)
--|							   - led(10) --> led(15) = MOVING UP
--|							   - led(5)  --> led(0)  = MOVING DOWN
--|							   - ALL OFF		     = NOT MOVING
--|							 an(3:0)    --> seven-segment display anode active-low enable (AN3 ... AN0)
--|							 seg(6:0)	--> seven-segment display cathodes (CG ... CA.  DP unused)
--|
--| DOCUMENTATION : C3C Denver Dalpias helped us find an error in our w_floors_tens logic.
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std
--|    Files     : MooreElevatorController.vhd, clock_divider.vhd, sevenSegDecoder.vhd
--|				   thunderbird_fsm.vhd, sevenSegDecoder, TDM4.vhd, OTHERS???
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;


entity top_basys3 is
port(

clk     :   in std_logic; -- native 100MHz FPGA clock
-- Switches (16 total)
sw   :   in std_logic_vector(15 downto 0);
-- Buttons (5 total)
btnC : in std_logic;  -- GO
btnU : in std_logic;  -- master_reset
btnL : in std_logic;                    -- clk_reset
btnR : in std_logic;                  -- fsm_reset
--btnD : in std_logic; 
-- LEDs (16 total)
led :   out std_logic_vector(15 downto 0);

-- 7-segment display segments (active-low cathodes)
seg : out std_logic_vector(6 downto 0);

-- 7-segment display active-low enables (anodes)
an      : out std_logic_vector(3 downto 0)
);
end top_basys3;

architecture top_basys3_arch of top_basys3 is 
  
-- declare components and signals
component clock_divider is
        generic ( constant k_DIV : natural := 2    );
        port (     i_clk    : in std_logic;           --clk for MooreElevatorController, TDM, and Thunderbird
                   o_clk    : out std_logic;           
                   i_reset  : in std_logic
        );
end component clock_divider;

component MooreElevatorController is
    Port ( i_clk    : in  STD_LOGIC;
           i_stop    : in  STD_LOGIC;
           i_up_down : in  STD_LOGIC;
           i_reset   : in std_logic;
           o_floor  : out STD_LOGIC_VECTOR (3 downto 0)     
);
end component MooreElevatorController;

component sevenSegDecoder is 
  port(
    i_D : in std_logic_vector(3 downto 0);
    o_S : out std_logic_vector(6 downto 0)
-- Identify input and output bits here
);
 end component sevenSegDecoder;
 
 component thunderbird_FSM is
   port( i_clk, i_reset     : in  STD_LOGIC;                 
         i_left, i_right    : in  STD_LOGIC;
         o_lights_L         : out  STD_LOGIC_vector(2 downto 0);  --logic vectors because each need 6 LEDs
         o_lights_R         : out  STD_LOGIC_vector(2 downto 0)
         );
 end component thunderbird_FSM;
 
 component TDM4 is 
    generic ( constant k_WIDTH : natural  := 4); -- bits in input and output
     Port ( i_CLK         : in  STD_LOGIC;
            i_RESET         : in  STD_LOGIC; -- asynchronous
            i_D3         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
            i_D2         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
            i_D1         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
            i_D0         : in  STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
            o_DATA        : out STD_LOGIC_VECTOR (k_WIDTH - 1 downto 0);
            o_SEL        : out STD_LOGIC_VECTOR (3 downto 0)    -- selected data line (one-cold)
     );
 end component TDM4;
 
 signal w_clk : std_logic := '0';
 signal w_floor : std_logic_vector(3 downto 0);
 signal w_data : std_logic_vector(3 downto 0);
 signal w_fsm : std_logic := '0';
 signal w_clkdiv : std_logic := '0';
 signal w_tbird : std_logic := '0';
 signal w_up_floors : std_logic := '0';
 signal w_down_floors : std_logic := '0';
 signal w_clk_lights : std_logic := '0';
 signal w_clk_TDM4 : std_logic := '0';
 signal w_lightsR : std_logic_vector(2 downto 0);
 signal w_lightsL : std_logic_vector(2 downto 0);
 signal w_floors_tens : std_logic_vector(3 downto 0) := "0000";
 signal w_floors_ones : std_logic_vector(3 downto 0) := "0000";
 signal w_stop  : std_logic := '0';
 signal w_DesiredFloor : std_logic_vector(3 downto 0) := "0001";
 signal w_upDown : std_logic := '0';
 signal w_FirstFloor : std_logic_vector(3 downto 0) := "0000";
 
 
begin
-- PORT MAPS ----------------------------------------
    clkdiv_inst : clock_divider --instantiation of clock_divider to take 
            generic map ( k_DIV => 25000000 ) -- 2 Hz clock from 100 MHz
            port map (                          
                i_clk   => clk,
                o_clk   => w_clk,   --clk instance for MooreElevatorController
                i_reset => w_clkdiv
            );
    clkdiv2_inst : clock_divider
           generic map( k_DIV => 6250000)
           port map( 
                     i_clk => clk,     --clk instance for TDM4
                     i_reset => w_clkdiv,
                     o_clk => w_clk_lights
           );
    clkdiv3_inst : clock_divider
                      generic map( k_DIV => 50000)
                      port map( 
                                i_clk => clk,
                                i_reset => w_clkdiv,   --clk instance for ThunderBird Lights
                                o_clk => w_clk_TDM4
                      );
          
    MooreElevatorController_inst : MooreElevatorController
         port map (
               i_clk => w_clk,
               i_up_down => w_upDown,
               i_stop => w_stop,
               i_reset => w_fsm,
               o_floor => w_floor
          );
    SevSegDecoder_inst : SevenSegDecoder
        port map (
               i_D => w_data,
               o_S => seg
        );
        
      Thunderbird_inst : thunderbird_fsm
         port map(
               i_clk => w_clk_lights,
               i_reset => w_tbird,
               i_left => w_up_floors,
               i_right => w_down_floors,
               o_lights_L(2) => w_lightsL(2),
               o_lights_L(1) => w_lightsL(1),
               o_lights_L(0) => w_lightsL(0),
               o_lights_R(2) => w_lightsR(2),
               o_lights_R(1) => w_lightsR(1),
               o_lights_R(0) => w_lightsR(0)
               );
               
      TDM4_inst : TDM4
        port map(
            i_CLK => w_clk_TDM4,
            i_RESET => w_fsm,
            i_D2 => w_floors_ones, --will be the second digit
            i_D3 => w_floors_tens, --will be the first digit
            i_D1 => "0000", --initialize these to "0000" because we only have two data sets
            i_D0 => "0000",
            o_DATA => w_data,
            o_SEL => an
            );
            
-- CONCURRENT STATEMENTS ----------------------------
         an(0) <= '1';  --active low, so initialize these to '1' to keep them off
         an(1) <= '1';
         
         
    w_fsm <= btnR or btnU;
    w_clkdiv <= btnL or btnU; --reset logic
    w_tbird <= btnR or btnU;
   
   w_FirstFloor <= sw(15 downto 12);   --stores input for passenger pickup
   w_DesiredFloor <= sw(3 downto 0) when (w_floor = w_FirstFloor) else  --takes input for desired floor after reaching the floor for passenger pickup
                      w_FirstFloor when (btnC = '1'); --only gos when btnC is pressed
                      
   
   w_up_floors <= '1' when (w_upDown = '1') and (w_stop = '0') and (w_floor /= w_DesiredFloor);  --logic for lights
   w_down_floors <= '1' when (w_upDown = '0') and (w_stop = '0') and (w_floor /= w_DesiredFloor);   

 w_floors_tens <= "0001" when (w_floor = x"A") or (w_floor = x"B") or (w_floor = x"C") or (w_floor = x"D") or (w_floor = x"E") or (w_floor = x"F") else
                  "0000"; 
 w_floors_ones <= "0001" when (w_floor = x"1") else
                  "0010" when (w_floor = x"2") else
                  "0011" when (w_floor = x"3") else
                  "0100" when (w_floor = x"4") else
                  "0101" when (w_floor = x"5") else
                  "0110" when (w_floor = x"6") else
                  "0111" when (w_floor = x"7") else
                  "1000" when (w_floor = x"8") else
                  "1001" when (w_floor = x"9") else
                  "0000" when (w_floor = x"A") else   --converting from hex to binary
                  "0001" when (w_floor = x"B") else  
                  "0010" when (w_floor = x"C") else   
                  "0011" when (w_floor = x"D") else  
                  "0100" when (w_floor = x"E") else 
                  "0101" when (w_floor = x"F") else
                  "0000";  
                   
   w_stop <= '1' when (w_floor = w_DesiredFloor) else         --elevator should stop when it reached the desired floor
             '0';
   w_upDown <= '1' when ((w_DesiredFloor > w_floor) = true) else   --the Up lights should go on when Desired Floor is above the current floor
               '0';
   
       (led(5)) <= w_lightsR(0) or w_lightsR(1) or w_lightsR(2);   --connect two LEDs for every 1
       (led(4)) <= w_lightsR(0) or w_lightsR(1) or w_lightsR(2);
       (led(3)) <= w_lightsR(1) or w_lightsR(2);
       (led(2)) <= w_lightsR(1) or w_lightsR(2);
       (led(1)) <= w_lightsR(2);
       (led(0)) <= w_lightsR(2);
       
       led(10) <= w_lightsL(0) or w_lightsL(1) or w_lightsL(2);
       led(11) <= w_lightsL(0) or w_lightsL(1) or w_lightsL(2);
       led(12) <= w_lightsL(1) or w_lightsL(2);
       led(13) <= w_lightsL(1) or w_lightsL(2);
       led(14) <= w_lightsL(2);
       led(15) <= w_lightsL(2);
         
     
-- ground unused LEDs (which is all of them for Minimum functionality)
led(9 downto 6) <= (others => '0');

end top_basys3_arch;

