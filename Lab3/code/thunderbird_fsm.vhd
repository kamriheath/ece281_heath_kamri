--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : thunderbird.vhd
--| AUTHOR(S)     : C3C Kamri Heath
--| CREATED       : 03/18/2021, Last Modified 03/25/2021
--| DESCRIPTION   : This represents the logic behind the Thunderbird car's blinkers.
--|
--|
--| DOCUMENTATION : Refer to top_basys3
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std
--|    Files     : None
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

  
entity thunderbird_fsm is
    Port ( i_clk, i_reset     : in  STD_LOGIC;                 
           i_left, i_right    : in  STD_LOGIC;
           o_lights_L         : out  STD_LOGIC_vector(2 downto 0);  --logic vectors because each need 3 LEDs
           o_lights_R         : out  STD_LOGIC_vector(2 downto 0)
           );
end thunderbird_fsm;

architecture thunderbird_fsm_arch of thunderbird_fsm is 
	
	signal f_Q : std_logic_vector(7 downto 0) := "00000001";
	signal f_Q_next : std_logic_vector(7 downto 0) := "00000001";
  
begin
	-- CONCURRENT STATEMENTS ----------------------------
	-- Next state logic
	f_Q_next(0) <= (f_Q(0) AND not(i_left) AND not(i_right)) or f_Q(1) or f_Q(4) or f_Q(7);
	f_Q_next(1) <= f_Q(0) AND i_left AND i_right;
	f_Q_next(2) <= f_Q(0) AND not(i_left) AND i_right;
	f_Q_next(3) <= f_Q(2);
	f_Q_next(4) <= f_Q(3);                             --taken from preLab
	f_Q_next(5) <= f_Q(0) AND i_left AND not(i_right);
	f_Q_next(6) <= f_Q(5);
	f_Q_next(7) <= f_Q(6);
	
	-- Output logic
	    o_lights_L(2) <= f_Q(1) or f_Q(7);
        o_lights_L(1) <= f_Q(1) or f_Q(6) or f_Q(7);    
        o_lights_L(0) <= f_Q(1) or f_Q(5) or f_Q(6) or f_Q(7);
        o_lights_R(2) <= f_Q(1) or f_Q(4);                        --taken from preLab
        o_lights_R(1) <= f_Q(1) or f_Q(3) or f_Q(4);
        o_lights_R(0) <= f_Q(1) or f_Q(2) or f_Q(3) or f_Q(4);
	-------------------------------------------------------	
	
	-- PROCESSES ----------------------------------------	
	-- state memory w/ asynchronous reset ---------------
	register_proc : process (i_clk, i_reset)
	begin
	    if i_reset = '1' then
	       f_Q <= "00000001";
	    elsif (rising_edge(i_clk)) then
	       f_Q <= f_Q_next;
	    end if;
			--Reset is all lights off


	end process register_proc;
	-------------------------------------------------------
	
end thunderbird_fsm_arch;
