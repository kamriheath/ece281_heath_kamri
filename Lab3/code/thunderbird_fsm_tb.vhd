--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2017 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : thunderbird_tb.vhd (TEST BENCH)
--| AUTHOR(S)     : C3C Kamri Heath
--| CREATED       : 3/18/2021 Last modified 03/25/2021
--| DESCRIPTION   : This file provides a solution testbench for the thunderbird entity
--|
--| DOCUMENTATION : Refer to top_basys3
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std, unisim
--|    Files     : stoplight.vhd
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
 
entity thunderbird_fsm_tb is
end thunderbird_fsm_tb;
 
architecture behavior of thunderbird_fsm_tb is 
 
    component thunderbird_fsm
    port(
        i_clk, i_reset     : in  STD_LOGIC;
        i_left, i_right    : in  STD_LOGIC;
        o_lights_L         : out  STD_LOGIC_vector(2 downto 0);
        o_Lights_R         : out  STD_LOGIC_vector(2 downto 0)
        );
    end component;
    

	--Inputs
	signal i_reset : std_logic := '0';
	signal i_clk : std_logic := '0';
	signal i_left : std_logic := '0';
	signal i_right : std_logic := '0';
	
		--Outputs
	signal o_lights_L : std_logic_vector(2 downto 0);
	signal o_lights_R : std_logic_vector(2 downto 0);
	
	-- Clock period definitions
	constant k_clk_period : time := 10 ns;
 
begin
  	-- PORT MAPS ---------------------------------------------------
   uut: thunderbird_fsm port map (
             i_reset => i_reset,
             i_clk => i_clk,
             i_left => i_left,
             i_right => i_right,
             o_lights_L(2) => o_lights_L(2),
             o_lights_L(1) => o_lights_L(1),
             o_lights_L(0) => o_lights_L(0),
             o_lights_R(2) => o_lights_R(2),
             o_lights_R(1) => o_lights_R(1),
             o_lights_R(0) => o_lights_R(0)
             
          
        );
	----------------------------------------------------------------
  
	-- PROCESSES --------------------------------------------------- 
	-- Clock process
	clk_proc : process
	begin
		i_clk <= '0';
		wait for k_clk_period/2;
		i_clk <= '1';
		wait for k_clk_period/2;
	end process;
	
	sim_proc: process
	begin
		-- sequential timing		
		i_reset <= '1';
		wait for k_clk_period*1;
		
		i_reset <= '0';
		wait for k_clk_period*1;
		
		
		
		-- alternative way of implementing Finite State Machine Inputs
		-- starts after "wait for" statements
		-- statements after this one start in paralell to this one
        i_reset <= '1';
        wait for k_clk_period*1;
        
        i_reset <= '0';                  --start with reset
        wait for k_clk_period*1;
        
        i_left <= '0';
        i_right <= '1';
        wait for k_clk_period*5;   --right lights going
        
        i_left <= '0';
        i_right <= '0';
        wait for k_clk_period*2;
        
        i_left <= '1';
        i_right <= '0';
        wait for k_clk_period*3;   --left light going
        
        i_left <= '0';
        i_right <= '0';
        wait for k_clk_period*2;
        
        i_left <= '1';
        i_right <= '1';                 -- show reset
        wait for k_clk_period*3;
        i_reset <= '1';                
        wait for k_clk_period*3;
        i_reset <= '0';
        wait for k_clk_period*1;
        
        i_left <= '0';
        i_right <= '1';
        wait for k_clk_period/4;
        
        i_left <= '1';
        i_right <= '1';
        wait for k_clk_period*3;
        
        i_left <= '0';
        i_right <= '1';
        wait for k_clk_period*(5.25);
        
        i_right <= '0';
        i_left <= '1';
        wait for k_clk_period*3;
        
        i_left <= '0';
        i_right <= '0';
	
		wait;
	end process;
	----------------------------------------------------------------
end;
