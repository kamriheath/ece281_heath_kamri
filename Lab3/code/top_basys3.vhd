--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : top_basys3.vhd
--| AUTHOR(S)     : C3C Kamri Heath
--| CREATED       : 03/18/2021 Modified: 03/25/2021 
--| DESCRIPTION   : This file implements the top level module for the solution for Thunderbird FSM.
--|
--| DOCUMENTATION : Me and my partner Kevin Carrig worked through the functionality of the test bench together.
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES : 
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std
--|    Files     : stoplight.vhd, clock_divider.vhd
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;


entity top_basys3 is
	port(
         clk     :   in std_logic;
         sw      :   in  std_logic_vector(15 downto 0);
         btnR     :   in  std_logic;
         btnL    :     in  std_logic;
         led     :   out std_logic_vector(15 downto 0) --need(15-13 and 2-0); all others will be grounded
	);
end top_basys3;

architecture top_basys3_arch of top_basys3 is 

--Declare thunderbird component here 


component clock_divider is
	generic ( constant k_DIV : natural := 2	);
	port ( 	i_clk    : in std_logic;		   -- basys3 clk
			i_reset  : in std_logic;		   -- asynchronous
			o_clk    : out std_logic		   -- divided (slow) clock
	);
end component clock_divider;

component thunderbird_fsm is
    port(  i_left, i_right : in std_logic;
           i_Reset : in std_logic;
           i_clk : in std_logic;
           
           o_lights_L : out std_logic_vector(2 downto 0);  --3 leds for both left and right
           o_lights_R : out std_logic_vector(2 downto 0)
    );
end component thunderbird_fsm;

	signal w_clk : std_logic;		--this wire provides the connection between o_clk and thunderbird clk

begin
	-- PORT MAPS ----------------------------------------


	clkdiv_inst : clock_divider 		--instantiation of clock_divider to take 
        generic map ( k_DIV => 25000000 ) -- 4 Hz clock from 100 MHz
        port map (						  
            i_clk   => clk,
            i_reset => btnL,
            o_clk   => w_clk
        );  
        led(12 downto 3) <= (others => '0');  --grounding leds(12 through 3) 
    thunderbird_inst : thunderbird_fsm
         port map (
             i_reset => btnR,
             i_clk => w_clk,
             i_left => sw(15),       --if flipped, the left blinkers will begin the sequence
             i_right => sw(0),       --if flipped, the right blinker will begin the sequence
             o_lights_L(2) => led(15),
             o_lights_L(1) => led(14),
             o_lights_L(0) => led(13),
             o_lights_R(2) => led(0),
             o_lights_R(1) => led(1),
             o_lights_R(0) => led(2)
          );
          
               
	
end top_basys3_arch;
