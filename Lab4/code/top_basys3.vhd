--+----------------------------------------------------------------------------
--| 
--| COPYRIGHT 2018 United States Air Force Academy All rights reserved.
--| 
--| United States Air Force Academy     __  _______ ___    _________ 
--| Dept of Electrical &               / / / / ___//   |  / ____/   |
--| Computer Engineering              / / / /\__ \/ /| | / /_  / /| |
--| 2354 Fairchild Drive Ste 2F6     / /_/ /___/ / ___ |/ __/ / ___ |
--| USAF Academy, CO 80840           \____//____/_/  |_/_/   /_/  |_|
--| 
--| ---------------------------------------------------------------------------
--|
--| FILENAME      : top_basys3.vhd
--| AUTHOR(S)     : Capt Phillip Warner
--| CREATED       : 3/9/2018  MOdified by Capt Dan Johnson (3/30/2020)
--| DESCRIPTION   : This file implements the top level module for a BASYS 3 to 
--|					drive the Lab 4 Design Project (Advanced Elevator Controller).
--|
--|					Inputs: clk       --> 100 MHz clock from FPGA
--|							btnL      --> Rst Clk
--|							btnR      --> Rst FSM
--|							btnU      --> Rst Master
--|							btnC      --> GO (request floor)
--|							sw(15:12) --> Passenger location (floor select bits)
--| 						sw(3:0)   --> Desired location (floor select bits)
--| 						 - Minumum FUNCTIONALITY ONLY: sw(1) --> up_down, sw(0) --> stop
--|							 
--|					Outputs: led --> indicates elevator movement with sweeping pattern (additional functionality)
--|							   - led(10) --> led(15) = MOVING UP
--|							   - led(5)  --> led(0)  = MOVING DOWN
--|							   - ALL OFF		     = NOT MOVING
--|							 an(3:0)    --> seven-segment display anode active-low enable (AN3 ... AN0)
--|							 seg(6:0)	--> seven-segment display cathodes (CG ... CA.  DP unused)
--|
--| DOCUMENTATION : None
--|
--+----------------------------------------------------------------------------
--|
--| REQUIRED FILES :
--|
--|    Libraries : ieee
--|    Packages  : std_logic_1164, numeric_std
--|    Files     : MooreElevatorController.vhd, clock_divider.vhd, sevenSegDecoder.vhd
--|				   thunderbird_fsm.vhd, sevenSegDecoder, TDM4.vhd, OTHERS???
--|
--+----------------------------------------------------------------------------
--|
--| NAMING CONVENSIONS :
--|
--|    xb_<port name>           = off-chip bidirectional port ( _pads file )
--|    xi_<port name>           = off-chip input port         ( _pads file )
--|    xo_<port name>           = off-chip output port        ( _pads file )
--|    b_<port name>            = on-chip bidirectional port
--|    i_<port name>            = on-chip input port
--|    o_<port name>            = on-chip output port
--|    c_<signal name>          = combinatorial signal
--|    f_<signal name>          = synchronous signal
--|    ff_<signal name>         = pipeline stage (ff_, fff_, etc.)
--|    <signal name>_n          = active low signal
--|    w_<signal name>          = top level wiring signal
--|    g_<generic name>         = generic
--|    k_<constant name>        = constant
--|    v_<variable name>        = variable
--|    sm_<state machine type>  = state machine type definition
--|    s_<signal name>          = state name
--|
--+----------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;


entity top_basys3 is
	port(

		clk     :   in std_logic; -- native 100MHz FPGA clock
		
		-- Switches (16 total)
		sw  	:   in std_logic_vector(15 downto 0);
		
		-- Buttons (5 total)
		--btnC	:	in	std_logic;					  -- GO
		btnU	:	in	std_logic;					  -- master_reset
		btnL	:	in	std_logic;                    -- clk_reset
		btnR	:	in	std_logic;	                  -- fsm_reset
		--btnD	:	in	std_logic;			
		
		-- LEDs (16 total)
		led 	:   out std_logic_vector(15 downto 0);

		-- 7-segment display segments (active-low cathodes)
		seg		:	out std_logic_vector(6 downto 0);

		-- 7-segment display active-low enables (anodes)
		an      :	out std_logic_vector(3 downto 0)
	);
end top_basys3;

architecture top_basys3_arch of top_basys3 is 
  
	-- declare components and signals
component clock_divider is
        generic ( constant k_DIV : natural := 2    );
        port (     i_clk    : in std_logic;           -- basys3 clk
                   o_clk    : out std_logic;           -- divided (slow) clock
                   i_reset  : in std_logic
        );
end component clock_divider;

component MooreElevatorController is
    Port ( i_clk     : in  STD_LOGIC;
           i_stop    : in  STD_LOGIC;
           i_up_down : in  STD_LOGIC;
           i_reset   : in std_logic;
           o_floor   : out STD_LOGIC_VECTOR (3 downto 0)		   
		 );
end component MooreElevatorController;

component sevenSegDecoder is 
  port(
    i_D : in std_logic_vector(3 downto 0);
    o_S : out std_logic_vector(6 downto 0)
	-- Identify input and output bits here
	);
 end component sevenSegDecoder;
 
 component thunderbird_FSM is
   port( i_clk, i_reset     : in  STD_LOGIC;                 
         i_left, i_right    : in  STD_LOGIC;
         o_lights_L         : out  STD_LOGIC_vector(2 downto 0);  --logic vectors because each need 6 LEDs
         o_lights_R         : out  STD_LOGIC_vector(2 downto 0)
         );
 end component thunderbird_FSM;
 
 signal w_clk : std_logic := '0';
 signal w_floor : std_logic_vector(3 downto 0);
 signal w_7SD_EN_n : std_logic := '0';
 signal r_fsm : std_logic := '0';
 signal r_clkdiv : std_logic := '0';
 signal r_tbird : std_logic := '0';
 signal w_up_floors : std_logic := '0';
 signal w_down_floors : std_logic := '0';
 signal w_clk_lights : std_logic := '0';
 signal w_lightsR : std_logic_vector(2 downto 0);
 signal w_lightsL : std_logic_vector(2 downto 0);
  
begin
	-- PORT MAPS ----------------------------------------
    clkdiv_inst : clock_divider 		--instantiation of clock_divider to take 
            generic map ( k_DIV => 25000000 ) -- 2 Hz clock from 100 MHz
            port map (                          
                i_clk   => clk,
                o_clk   => w_clk,
                i_reset => r_clkdiv
            );
    clkdiv2_inst : clock_divider
           generic map( k_DIV => 6000000)
           port map( i_clk => clk,
                     i_reset => r_clkdiv,
                     o_clk => w_clk_lights
                     );
          
    MEC_inst : MooreElevatorController
         port map (
               i_clk => w_clk,
               i_up_down => sw(1),
               i_stop => sw(0),
               i_reset => r_fsm,
               o_floor => w_floor
          );
    SevSegDecoder_inst : SevenSegDecoder
        port map (
               i_D => w_floor,
               o_S => seg
        );
        
      Thunderbird_inst : thunderbird_fsm
         port map(
               i_clk => w_clk,
               i_reset => r_tbird,
               i_left => w_up_floors,
               i_right => w_down_floors,
               o_lights_L(2) => w_lightsL(2),
               o_lights_L(1) => w_lightsL(1),
               o_lights_L(0) => w_lightsL(0),
               o_lights_R(2) => w_lightsR(2),
               o_lights_R(1) => w_lightsR(1),
               o_lights_R(0) => w_lightsR(0)
               );
               
   w_7SD_EN_n <= '0';  --connect button to wire(anode)
            an(0) <= '1';
            an(1) <= '1';
            an(2) <= w_7SD_EN_n;
            an(3) <= '1';  
            
    r_fsm <= btnR or btnU;
    r_clkdiv <= btnL or btnU;
    r_tbird <= btnR or btnU;
    
   --thunderbird_proc : process(w_floor, sw(0), sw(1), w_up_floors, w_down_floors) 
   --begin
        --if(sw(0) = '1') then 
           --w_up_floors <= '0';
           --w_down_floors <= '0';
        --end if; 
        --if(sw(0) /= '1' and sw(0) = '0' and w_floor /= "0001") then 
           --w_up_floors <= '0';
           --w_down_floors <= '1';
        --end if;
        --if(sw(0) /= '1' and sw(1) = '1' and w_floor /= "0100") then 
            --w_up_floors <= '1';
            --w_down_floors <= '0';
        --end if;
        
   --end process thunderbird_proc;
    
   w_up_floors <= sw(1) and not sw(0) and not (w_floor(3));
   w_down_floors <= not sw(1) and not sw(0) and not(w_floor(1));
   
       (led(5)) <= w_lightsR(0) or w_lightsR(1) or w_lightsR(2);
       (led(4)) <= w_lightsR(0) or w_lightsR(1) or w_lightsR(2);
       (led(3)) <= w_lightsR(1) or w_lightsR(2);
       (led(2)) <= w_lightsR(1) or w_lightsR(2);
       (led(1)) <= w_lightsR(2);
       (led(0)) <= w_lightsR(2);
       
       led(10) <= w_lightsL(0) or w_lightsL(1) or w_lightsL(2);
       led(11) <= w_lightsL(0) or w_lightsL(1) or w_lightsL(2);
       led(12) <= w_lightsL(1) or w_lightsL(2);
       led(13) <= w_lightsL(1) or w_lightsL(2);
       led(14) <= w_lightsL(2);
       led(15) <= w_lightsL(2);
         
      
	
	
	-- CONCURRENT STATEMENTS ----------------------------
	
	-- ground unused LEDs (which is all of them for Minimum functionality)
	led(9 downto 6) <= (others => '0');

	-- leave unused switches UNCONNECTED
	
	-- Ignore the warnings associated with these signals
	-- Alternatively, you can create a different board implementation, 
	--   or make additional adjustments to the constraints file
	
	-- wire up active-low 7SD anodes (an) as required
	-- Tie any unused anodes to power ('1') to keep them off
	
end top_basys3_arch;
